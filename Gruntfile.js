'use strict';

module.exports = function(grunt) {

  grunt.initConfig({

    pkg: grunt.file.readJSON('package.json'),

    project: {
      app: ['./'],
      //assets: ['<%= project.app %>/assets'],
      css: ['<%= project.app %>/css/layout.scss'],
      bootstrap: ['<%= project.app %>/css/bootstrap.scss'],
      mixins: ['<%= project.app %>/css/mixins.scss']
    },

    sass: {
      dev: {
        options: {
          style: 'expanded',
          compass: false
        },
        files: {
          '<%= project.app %>/css/bootstrap.css':'<%= project.bootstrap %>',
          '<%= project.app %>/.tmp/layout.css':'<%= project.css %>',
          '<%= project.app %>/css/mixins.css':'<%= project.mixins %>'
        }
      }
    },

    autoprefixer: {
      dist: {
        files : {
          '<%= project.app %>/css/layout.css': '<%= project.app %>/.tmp/layout.css'
        }
      }
    },

    watch: {
      sass: {
        files: [
          '<%= project.app %>/css/{,*/}*.{scss,sass}',
          '<%= project.app %>/css/*.scss'
          ],
        tasks: ['sass:dev','autoprefixer']
      }
    },

    browserSync: {
      dev: {
        bsFiles: {
          src : [
            './css/*.scss',
            './css/layout.css',
            '*.html',
            './js/*.js',
            //'./css/_careers-new.scss'
            //'./views/*.html',
            //'./components/*.html',
            //'./scripts/controllers/*.js',
            //'./scripts/factories/*.js',
            //'./scripts/directives/*.js'
          ]
        },
        options: {
          watchTask: true,
          server: {
            baseDir: "./"
          }
          //,
          //rewriteRules: [
          //  {
          //    match: /(cats|kitten[sz]) are mediocre/g,
          //    replace: "$1 are excellent"
          //    //match: /(.*)/g,
          //    //replace: "test"
          //  }
          //]
          //proxy: "local.sfoutsitelands.com:8888"
        }
      }
    }

  });

  // Load
  grunt.loadNpmTasks('grunt-contrib-sass');
  grunt.loadNpmTasks('grunt-autoprefixer');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-browser-sync');
  grunt.loadNpmTasks('grunt-contrib-clean');
  //grunt.loadNpmTasks('grunt-prerender');
  //grunt.loadNpmTasks('grunt-sitemap');


  /// Register
  //grunt.registerTask('default', ['sass','autoprefixer','browserSync','watch']);

  //default grunt task without sass
  grunt.registerTask('default', ['autoprefixer','browserSync','watch']);

  //grunt.registerTask('sitemap', ['sitemap']);

  //grunt.registerTask('default', [
  //  'sass','autoprefixer','browserSync','watch'
  //]);

};
